from application import APP

@APP.route('/')
def hello_world():
    return 'Hello, World!'

if __name__ == "__main__":
    from flask_cors import CORS
    CORS(APP)
    APP.run(host='0.0.0.0')