FROM nginx:latest

RUN apt-get update && apt-get install -y python3 vim python3-pip
COPY requirements.txt /requirements.txt
RUN pip3 install -r requirements.txt && rm requirements.txt && mkdir /app
COPY myapp.py /
RUN rm /etc/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/conf.d/py.conf /etc/nginx/conf.d/py.conf
#COPY nginx/conf.d/redirect.conf /etc/nginx/conf.d/redirect.conf
COPY uswgi.ini /uswgi.ini
COPY startup.sh /startup.sh

# generating the ssl/tls certs
RUN mkdir -p /etc/openssl/tls
RUN openssl req -x509 -nodes -days 1095 -newkey rsa:2048 -out /etc/openssl/tls/server.crt -keyout /etc/openssl/tls/server.key \
-subj "/C=US/ST=SC/L=Greenville/O=Internet Widgits Pty Ltd/CN=example.com/emailAddress=pfhamod@gmail.com"
COPY Certificates_PKCS7_v5.6_DoD /etc/openssl/tls/Certificates_PKCS7_v5.6_DoD
RUN openssl pkcs7 -in /etc/openssl/tls/Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD.pem.p7b -print_certs \
    -out /etc/openssl/tls/alldodcerts.pem
RUN ls /etc/openssl/tls/ 

CMD [ "/startup.sh" ]