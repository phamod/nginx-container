# nginx-container

This is an example that is meant to replace the [apache container](https://gitlab.com/phamod/apache2-container). Nginx was created as a replacement to apache and with migration to container architectures there is almost no benefit to use apache over nginx.

## Quick start
building the image run the following command.  
`docker build -t nginx-server .`  
the following command will run the python server and allow a test port for testing the tls encryption is not the problem.  
`docker run -p 5000:5000 -p 443:443 --name nginx-server -d --restart-always nginx-server`
