from flask import Flask, Blueprint

app = Flask(__name__)
bp = Blueprint('main', __name__)

@bp.route('/hello')
def hello_world():
    return 'Hello, World!'

app.register_blueprint(bp, url_prefix="/api/v1")

if __name__ == "__main__":
    app.run(host="0.0.0.0")